# source2image golang

[Source2image](https://github.com/openshift/source-to-image) golang template. Seems like a magic.

## Build

    s2i build https://gitlab.com/source2image/golang.git registry.gitlab.com/source2image/golang s2i-go-example --loglevel 1

## Run

    docker run -d --rm --name s2i-go-example -p 8080:8080 s2i-go-example

## Test

    curl -s http://127.0.0.1:8080/s2i

